# GG(S)NN

An implementation of various GG(S)NNs in PyTorch, based off [these Lua implementations](https://github.com/yujiali/ggnn)
